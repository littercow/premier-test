﻿using System;

namespace lab2_2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            //exercise 2.5
            
            Console.WriteLine("Entrez un nombre : ");
            int nombre = int.Parse(Console.ReadLine());
            if(nombre%5==0 && nombre % 7 != 0)
            {
                Console.WriteLine("Le nombre "+nombre+" est valide");
            }
            else
            {
                Console.WriteLine("Le nombre " + nombre + " est invalide"); //Completement vrai
            }

            Console.ReadKey(); //Peut causer des spasmes
            
        }
    }
}
